import * as actionTypes from '../constants/types'

export const getProduct = () => {
	return {
		type : actionTypes.FETCH_PRODUCTS,
		payload: {
			currentPage: 1,
			perPage: 16 
		}
	}
}

export const getProductSuccess = (products) => {
	return {
		type : actionTypes.FETCH_PRODUCTS_SUCCESS,
		payload: products
	}
}