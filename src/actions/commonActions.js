import * as actionTypes from '../constants/types'

export const getProduct = () => {
	return {
		type : actionTypes.FETCH_PRODUCTS
	}
}