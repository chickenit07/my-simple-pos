import * as actionTypes from "../constants/types";
import { getProductSuccess } from "../actions/productAction";
import { ajax } from "rxjs/ajax";
import { map, mergeMap } from "rxjs/operators";
import { ofType } from "redux-observable";

const baseUrl = 'http://localhost.com/rest/V1/';
const fetchProductEpic = (action$, state$) =>
  action$.pipe(
	ofType(actionTypes.FETCH_PRODUCTS),
    mergeMap((action) =>
      ajax
        .getJSON(
			 baseUrl + `getListProducts?searchCriteria[pageSize]=${action.payload.perPage}&searchCriteria[currentPage]=${action.payload.currentPage}`
        )
        .pipe(
          map((response) => {
			  let rs = response.items.map((product) => ({
				  id: product.id,
				  sku: product.sku,
				  name: product.name,
				  price: product.price,
				  image: product.custom_attributes[0].value,
				}));
            return getProductSuccess(rs);
          })
        )
    )
  );
export default fetchProductEpic;
