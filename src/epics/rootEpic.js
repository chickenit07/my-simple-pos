import { combineEpics } from "redux-observable";
import "./productEpics";
import fetchProductEpic from "./productEpics";

const rootEpic = combineEpics(
 	fetchProductEpic,
);
export default rootEpic;
