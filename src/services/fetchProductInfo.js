const baseUrl = "http://localhost.com/rest/V1/";

export const fetchProductInfo = async (perPage, currentPage) => {
  const productResult = await fetch(
    baseUrl +
      "getListProducts?" +
      "searchCriteria[pageSize]=" + perPage +
      "&searchCriteria[currentPage]=" + currentPage
  );
  const productData = await productResult.json();

  const productInfo = productData.map((product) => {
	  return {
		  id: product.id,
		  sku: product.sku,
		  name: product.name,
		  price: product.price,
		  image: product.custom_attributes[0].value,
		};
	})

	console.log(productInfo);
 	return productInfo ;
};

export const fetchCurrency = async () => {
  const currencyResult = await fetch(baseUrl + "getCurrencySymbol");
  const currencyData = await currencyResult.json();

  return currencyData;
};
