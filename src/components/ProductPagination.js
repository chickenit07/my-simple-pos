import React, { Component } from "react";
import { Button, Pagination } from "react-bootstrap";

export default class ProductPagination extends Component {
  constructor() {
    super();
    this.state = {
      currentPage: 1,
    };
  }

  toPrevPage = () => {
    if (this.state.currentPage > 1)
	this.setState({ currentPage: this.state.currentPage - 1 });
  };

  toNextPage = () => {
    this.setState({ currentPage: this.state.currentPage + 1 });
  };

  render() {
    return (
      <div>
        <Button onClick={this.toPrevPage}>Prev</Button>
        <Button onClick={this.toNextPage}>Next</Button>
      </div>
    );
  }
}
