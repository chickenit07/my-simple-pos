import React from 'react'
import { Button } from 'react-bootstrap'

export default function Login(props) {
	// console.log(props);
	return (
		<div>
			<Button onClick={props.handleLogin}>
				Login
			</Button>
		</div>
	)
}
