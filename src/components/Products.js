import React, { Component } from "react";
import { formatCurrency, formatMediaUrl, formatSku } from "../constants/utils";
import ProductPagination from "./ProductPagination";
import { connect } from "react-redux";
// import * as productAction from "../actions/productAction";

const mapStateToProps = (state) => {
  return {
	...state.products
  };
};

const mapDispatchToProps = (dispatch) => {
	return {
	  
	};
  };
class Products extends Component {
  render() {
    return (
      <div>
        <ul className="products">
          {this.props.products &&
            this.props.products.map((product) => (
              <li key={product.id}>
                <div className="product">
                  <a href={"#" + product.id}>
                    <img
                      src={formatMediaUrl(product.image)}
                      alt={product.name}
                    ></img>
                    <div>{product.name}</div>
                    <div>{formatSku(product.sku)}</div>
                    <div>
                      <span>
                        {" "}
                        {this.props.currency + formatCurrency(product.price)}
                      </span>
                      {/* <span> Avail: {this.props.avail} </span> */}
                    </div>
                  </a>
                </div>
              </li>
            ))}
        </ul>
        <ProductPagination></ProductPagination>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
