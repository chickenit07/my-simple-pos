import React from "react";
import { connect } from "react-redux";
import * as productAction from "../actions/productAction";

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchingProduct: () => {
      dispatch(productAction.getProduct());
    },
  };
};
function Loading(props) {
  const fetchProductInfo = () => {
    props.onFetchingProduct();
  };

  const handleLogin = () => {
    fetchProductInfo();
  };
  return (
    <div>
      <button onClick={handleLogin()}>click me to fetch api</button>;
    </div>
  );
}

export default connect(null, mapDispatchToProps)(Loading);
