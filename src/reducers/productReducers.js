import * as actionTypes from "../constants/types";

const initState = {
  isLoading: true,
  products: [],
  perPage: 16,
  currentPage: 1
};

export const productReducers = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PRODUCTS:
      return {
		  ...state,
        isLoading: false
	  };
	  
    case actionTypes.FETCH_PRODUCTS_SUCCESS:
      return {
		  ...state,
		  products: action.payload
	  };
    default:
      return state;
  }
};
