import { MEDIA_PATH } from "./matcher";


export function formatCurrency(num){
	return Number(num.toFixed(true)).toLocaleString() + " ";
}

export function formatMediaUrl(url){
	return MEDIA_PATH.concat(url);
}

export function formatSku(sku){
	return "[" + sku + ']';
}