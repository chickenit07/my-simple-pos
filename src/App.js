import React from "react";
import Products from "./components/Products";
import Header from "./components/Header";
import Cart from "./components/Cart";
import Loading from "./components/Loading";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    ...state.products,
  };
};
class App extends React.Component {
  render() {
    return (
      <div>
        {this.props.isLoading ? (
          <Loading></Loading>
        ) : (
          <div className="container">
            <Header></Header>
            <main>
              <div className="content">
                <div className="cart">
                  <Cart></Cart>
                </div>
                <div className="product-list">
                  <Products></Products>
                </div>
              </div>
            </main>
          </div>
        )}
      </div>
    );
  }
}

export default connect(mapStateToProps, null)(App);
