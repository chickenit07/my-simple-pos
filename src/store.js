import rootReducer from "./reducers";
import { createStore } from "redux";
import rootEpic from "./epics/rootEpic";
import { createLogger } from "redux-logger";
import { applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";

const epicMiddleware = createEpicMiddleware();

export default function configureStore() {
  const store = createStore(
    rootReducer,
    applyMiddleware(createLogger(), epicMiddleware)
  );

  epicMiddleware.run(rootEpic);

//   store.dispatch(getProduct());
  
  return store;
}
