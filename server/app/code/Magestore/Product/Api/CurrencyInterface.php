<?php


namespace Magestore\Product\Api;


interface CurrencyInterface
{
    /**
     * Retrieve default currency code
     *
     * @return string
     */
    public function getDefaultCurrency();

    /**
     * @return  \Magento\Framework\Currency
     */
    public function getCurrencySymbol();
}
