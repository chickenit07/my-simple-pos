<?php
/**
 * Currency
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Product\Helper;


class Currency
{
    protected $currencyFactory;
    protected $storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->currencyFactory=$currencyFactory;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCurrencySymbol(){
        $currencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode();
        $currencySymbol =  $this->currencyFactory->create()->load($currencyCode)->getCurrencySymbol();

        return $currencySymbol;
    }
}
